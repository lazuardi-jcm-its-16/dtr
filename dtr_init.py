"""
initialisation

callable functions from this file:

    block_dist
    check_arguments
    display_render_details

------------------------------------------------------------------------------
Copyright 2015-2017 Alan Taylor

This file is part of dtr.

dtr is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

dtr is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with dtr.  If not, see <http://www.gnu.org/licenses/>.
------------------------------------------------------------------------------
"""

import argparse                 # ArgumentParser
import concurrent.futures as cf # ProcessPoolExecutor
import datetime                 # timedelta
import functools                # partial
import itertools                # chain, combinations, permutations, product
import math                     # cos, pi, sqrt
import multiprocessing as mp    # cpu_count
import os                       # path.isdir, path.isfile
import sys                      # argv, exit

import dtr_benchmark as bm      # benchmark_new_nodes
import dtr_data_struct as ds    # FILENAME_USER_SETTINGS, RenderNode
import dtr_file_io as fio       # backup_render
import dtr_utils as utils       # despatch_order, no_dupes_ipu,
                                # remove_dead_nodes


##############################################################################
# decide the best way to split the image into blocks that we can despatch
# to render nodes
##############################################################################

def _factors(num):
    """
    calculate the factors of a given number

    --------------------------------------------------------------------------
    args
        num : int
    --------------------------------------------------------------------------
    returns
        result : set of int
    --------------------------------------------------------------------------
    """
    result = set()
    for poss in range(1, int(math.sqrt(num)) + 1):
        div, mod = divmod(num, poss)
        if mod == 0:
            result.update({poss, div})
    return result

def _perm_fact(sof):
    """
    given a set of factors, return a list of pairs factors whose product
    equals the largest factor in the set

    --------------------------------------------------------------------------
    args
        sof : (s)et (o)f (f)actors of a number, such as {16, 1, 2, 4, 8}
    --------------------------------------------------------------------------
    yields
        generated series of tuples (int, int)
            for example, matching the set given above:
            (4, 4), (16, 1), (1, 16), (2, 8), (8, 2)
    --------------------------------------------------------------------------
    """
    max_factor = max(sof)

    for fac in sof:
        if fac * fac == max_factor:
            yield (fac, fac)
            break

    for blk_dim in itertools.permutations(sof, 2):
        if blk_dim[0] * blk_dim[1] == max_factor:
            yield blk_dim

def _ar_uniform(settings, block):
    """
    return the ratio of a block's largest side to its smallest side, taking
    into account the size of a tile in pixels

    --------------------------------------------------------------------------
    args
        settings : image_config dictionary
            contains core information about the image to be rendered
        block : tuple (int, int)
            dimensions of the block in units of tiles
    --------------------------------------------------------------------------
    returns : float
    --------------------------------------------------------------------------
    """
    blk_x = block[0] * settings['tile_size_x']
    blk_y = block[1] * settings['tile_size_y']
    return max(blk_x, blk_y) / min(blk_x, blk_y)

def _block_size_pixels(settings, block):
    """
    return block size in pixels

    --------------------------------------------------------------------------
    args
        settings : image_config dictionary
            contains core information about the image to be rendered
        block : tuple (int, int)
            dimensions of the block in units of tiles
    --------------------------------------------------------------------------
    returns : int
    --------------------------------------------------------------------------
    """
    return block[0] * block[1] * settings['tile_size_x'] * settings['tile_size_y']

def _dist_norm(settings, block, centre, max_dist, data_point):
    """
    maps the range from centre (most desirable) to max_dist (least desirable)
    to a value between 1.0 (most desirable) and 0.0 (least desirable) using a
    cosine function

    --------------------------------------------------------------------------
    args
        settings : image_config dictionary
            contains core information about the image to be rendered
        block : tuple (int, int)
            dimensions of the block in units of tiles
        centre : int or float
            desired centre point of distribution
        max_dist : int or float
            the distance of the most extreme data point in the distribution
            from the desired centre
        data_point : function(block)
            any function that accepts a block as defined in this argument
            list, and returns a single numeric value (int or float), but
            designed to be used with _block_size_pixels and _ar_uniform
    --------------------------------------------------------------------------
    returns : float
    --------------------------------------------------------------------------
    """
    dist_from_centre = abs(centre - data_point(settings, block))
    return abs(math.cos((dist_from_centre * math.pi) / (2.0 * max_dist)))

def _bl1_get_block_sizes(settings, min_blocks, max_tiles_x, max_tiles_y):
    """
    return a list of tuples, each tuple being a plausible block size
    (width, height) in units of tiles

    --------------------------------------------------------------------------
    args
        settings : image_config dictionary
            contains core information about the image to be rendered
        min_blocks : int
            the minimum number of blocks to split the image into
        max_tiles_x : int
            the maximum number of tiles that fit across the image
        max_tiles_y : int
            the maximum number of tiles that fit down the image
    --------------------------------------------------------------------------
    returns
        good_fits : list of tuples [(int, int), (int, int), ...]
    --------------------------------------------------------------------------
    """

    # calculate plausible numbers of tiles per block. constrained to values
    # that give a number of blocks per image that's in an acceptable range
    image_pixels = settings['image_x'] * settings['image_y']
    num_tiles = image_pixels // (settings['tile_size_x'] * settings['tile_size_y'])
    max_tiles = num_tiles // settings['min_tiles_per_block']
    tpb_range = range(settings['min_tiles_per_block'], max_tiles + 1, settings['max_threads'])
    tiles_per_blk = (x for x in tpb_range if \
        num_tiles % x == 0 and min_blocks <= (num_tiles // x) <= settings['max_blocks'])

    # each tuple represents the block dimensions in units of tiles
    all_block_xy = itertools.chain.from_iterable(_perm_fact(_factors(x)) for x in tiles_per_blk)

    # filter out block dimensions that do not divide nicely into the image size
    good_fits = [x for x in all_block_xy if max_tiles_x % x[0] == max_tiles_y % x[1] == 0]

    return good_fits

def _block_layout_1(settings, min_blocks):
    """
    calculate the number of blocks to split the image in to, that will
    result in the render completing in the shortest possible time.

    it tries to find block sizes that are convenient multiples of the
    internally specified blender tile sizes (*) that fit perfectly within the
    image space; this helps keep all cores of the remote nodes fully occupied
    until the respective block is complete.

    this function provides a service to _block_layout() and is a partner
    function to _block_layout_2 and _block_layout_3.

    (*) see dict image_config in dtr.py:
            tile_size_x, tile_size_y, min_tiles_per_block, max_threads

    --------------------------------------------------------------------------
    args
        settings : image_config dictionary
            contains core information about the image to be rendered
        min_blocks : int
            the minimum number of blocks to split the image in to
    --------------------------------------------------------------------------
    returns
        bool : True if no solution found, False otherwise
        settings : image_config dictionary
            mutable type amended in place
    --------------------------------------------------------------------------
    """
    # return if the image dimensions are not divisible by the tile dimensions
    max_tiles_x, remainder_tx = divmod(settings['image_x'], settings['tile_size_x'])
    max_tiles_y, remainder_ty = divmod(settings['image_y'], settings['tile_size_y'])
    if remainder_tx == remainder_ty != 0:
        return True

    # block sizes that will form patterns that fit into the image space
    good_fits = _bl1_get_block_sizes(settings, min_blocks, max_tiles_x, max_tiles_y)
    if not good_fits:
        return True

    # rate block arrangements by how close their total number of pixels is to
    # the desired value.
    #
    # the 16384 constant yields block sizes that give an appropriate number
    # of blocks for given image sizes. The figures below assume 16 x 16 pixel
    # blender tiles, typical for CPU rendering.
    #
    # 7680 x 4320: 900 blocks of 256 * 144 pixels
    # 3840 x 2160: 450 blocks of 128 * 144 pixels
    # 2560 x 1440: 200 blocks of 128 * 144 pixels
    centre = 16384.0
    limit = max(_block_size_pixels(settings, x) for x in good_fits)
    np_dist = [_dist_norm(settings, x, centre, limit, _block_size_pixels) for x in good_fits]

    # rate block arrangements by how close their aspect ratio is to square
    # as this is more useful when evaluating previews of incomplete renders
    centre = 1.0
    limit = max(_ar_uniform(settings, x) for x in good_fits)
    ar_dist = [_dist_norm(settings, x, centre, limit, _ar_uniform) for x in good_fits]

    # choose the best option, based on number of pixels and aspect ratio
    best_fit = max(zip(good_fits, np_dist, ar_dist), key=lambda x: x[1] * x[2])[0]

    settings['blocks_x'] = max_tiles_x // best_fit[0]
    settings['blocks_y'] = max_tiles_y // best_fit[1]

    return False

def _block_arrangements(settings, min_blks):
    """
    generate a series of block sizes (in units of tiles) that meet basic
    acceptance criteria:
        not too thin
        not too small
        the numbers of tiles to split the image into is in the right range

    --------------------------------------------------------------------------
    args
        settings : image_config dictionary
            contains core information about the image to be rendered
        min_blocks : int
            the minimum number of blocks to split the image in to
    --------------------------------------------------------------------------
    yields
        tuples : ((int, int), float)
            e.g. one of the generated tuples for a 1280 * 720 image would be

            ((16, 9), 0.0)

            where the image is split into 16 units across and 9 units down,
            which gives a block size of 80 * 80 pixels.
            the block is square, so it's aspect ratio is 1.0; the value of the
            float in the tuple is the distance from 1.0, which is 0.0
    --------------------------------------------------------------------------
    """
    min_block_size_px = settings['tile_size_x'] * settings['tile_size_y'] * \
        settings['min_tiles_per_block']

    fact_x = _factors(settings['image_x'])
    fact_y = _factors(settings['image_y'])
    for x_px, y_px in itertools.product(fact_x, fact_y):
        blx = settings['image_x'] // x_px
        bly = settings['image_y'] // y_px
        image_blocks = blx * bly

        blk_not_thin = x_px >= settings['tile_size_x'] and y_px >= settings['tile_size_y']
        blk_not_tiny = x_px * y_px >= min_block_size_px
        num_blocks_okay = min_blks <= image_blocks <= settings['max_blocks']

        if blk_not_thin and blk_not_tiny and num_blocks_okay:
            yield ((blx, bly), abs(1.0 - x_px / y_px))

def _block_layout_2(settings, min_blocks):
    """
    calculate the number of blocks to split the image in to, that will
    result in the render completing in the shortest possible time.

    this function finds solutions where the image dimensions factorise,
    but are not convenient multiples of the minimum tile size.

    this function provides a service to _block_layout() and is a partner
    function to _block_layout_1 and _block_layout_3.

    --------------------------------------------------------------------------
    args
        settings : image_config dictionary
            contains core information about the image to be rendered
        min_blocks : int
            the minimum number of blocks to split the image into
    --------------------------------------------------------------------------
    returns
        settings : image_config dictionary
            mutable type amended in place, no explicit return
    --------------------------------------------------------------------------
    """
    # obtain list of possible solutions, ordered by how close they are to being square
    sol = sorted(_block_arrangements(settings, min_blocks), key=lambda x: x[1])

    if sol:
        squarest = sorted([x for x in sol if x[1] == sol[0][1]], key=lambda y: y[0][0] * y[0][1])
        found = squarest[-1]

        # store results
        settings['blocks_x'], settings['blocks_y'] = found[0]

        return False
    else:
        return True

def _block_layout_3(settings, min_blocks):
    """
    the method of calculation of last resort (!) for unusual image sizes
    that may have dimensions that are tiny, or have sides that do not
    factorise.

    this function may produce block sizes that when divided into the image
    dimensions, do not produce whole numbers of pixels. however blender, which
    uses floats to define the bounds of border renders, handles such things
    gracefully.  final composited renders will be perfectly seam free, just
    as with the other two methods.

    in essence, return a sane value for the number of blocks to render, as
    well as an arrangement for those blocks that makes sense given the shape
    of the final image

    this function provides a service to _block_layout() and is a partner
    function to _block_layout_1 and _block_layout_1.

    --------------------------------------------------------------------------
    args
        settings : image_config dictionary
            contains core information about the image to be rendered
        min_blocks : int
            the minimum number of blocks to split the image into
    --------------------------------------------------------------------------
    returns
        settings : image_config dictionary
            mutable type amended in place, no explicit return
    --------------------------------------------------------------------------
    """
    # prefer arrangements of h*v blocks, where either h or v = 1,
    # i.e. don't increase the number of blocks (so it has more factors),
    # unless the blocks become so narrow that we have no choice
    fact = _factors(min_blocks)
    image_aspect = settings['image_x'] / settings['image_y']
    x_wide_blocks_narrow = ((settings['image_x'] / min_blocks) < 8) and (image_aspect >= 1.0)
    y_tall_blocks_narrow = ((settings['image_y'] / min_blocks) < 8) and (image_aspect <= 1.0)
    if x_wide_blocks_narrow or y_tall_blocks_narrow:
        # increase the number of blocks until we can find at least 3 factors,
        # which will allow both h and v to be > 1
        while len(fact) < 3:
            min_blocks += 1
            fact = _factors(min_blocks)

    # choose a block arrangement based on similarity of shape to the image aspect ratio
    options = []
    aspect = []
    for n_blocks_y in fact:
        n_blocks_x = min_blocks // n_blocks_y
        block_aspect = n_blocks_x / n_blocks_y
        both_landscape = (image_aspect >= 1.0) and (block_aspect >= 1.0)
        both_portrait = (image_aspect <= 1.0) and (block_aspect <= 1.0)
        if both_landscape or both_portrait:
            options.append((n_blocks_x, n_blocks_y))
            aspect.append(block_aspect)

    closest_ar = min(aspect, key=lambda x: abs(x - image_aspect))
    index_closest = aspect.index(closest_ar)

    settings['blocks_x'], settings['blocks_y'] = options[index_closest]

def _block_layout(settings, min_blocks):
    """
    specify how to split the image into blocks

    the aligned solver will provide block sizes that will use the remote nodes
    most efficiently.

    the general solver is only used when either the image size is not
    divisible by the tile size or no suitable solution could be found by the
    aligned solver. the latter is most likely to occur where at least one of
    the following applies:

        * the image size is small
        * the image dimensions are unusual
        * there is a lot of variance in performance between nodes

    --------------------------------------------------------------------------
    args
        arn : list of RenderNode instances
            contains information about all the nodes in the cluster
        settings : image_config dictionary
            contains core information about the image to be rendered
        min_blocks : int
            the minimum number of tiles to split the image into
    --------------------------------------------------------------------------
    returns
        settings : image_config dictionary
            mutable type amended in place, no explicit return
    --------------------------------------------------------------------------
    """
    # if there is only one node, block size must equal image size
    if min_blocks == 1:
        settings['blocks_x'], settings['blocks_y'] = 1, 1
        return

    # (1) optimal solutions for popular resolutions
    if _block_layout_1(settings, min_blocks):

        # (2) decent solutions for less common resolutions
        if _block_layout_2(settings, min_blocks):

            # (3) produce something that will render given unusual dimensions
            _block_layout_3(settings, min_blocks)

def display_render_details(render_nodes, settings):
    """"
    show user the render configuration

    --------------------------------------------------------------------------
    args
        render_nodes : list of RenderNode instances
            contains information about all the nodes in the cluster
        settings : image_config dictionary
            contains core information about the image to be rendered
    --------------------------------------------------------------------------
    returns : none
    --------------------------------------------------------------------------
    """
    num_rn = len(render_nodes)
    suffix = '' if settings['blocks_required'] > 1 else \
        ' will be sent whole to the single available node'

    print(str(settings['image_x']) + ' * ' + str(settings['image_y']) + ' image, frame ' + \
        str(settings['frame']) + ' with seed ' + str(settings['seed']) + suffix)

    if settings['blocks_required'] > 1:

        print('the frame is split into ' + str(settings['blocks_required']) + \
            ' blocks (arranged as ' + str(settings['blocks_x']) + ' * ' + \
            str(settings['blocks_y']) + ') distributed over ' + str(num_rn) + ' node' + \
            ('s' if num_rn > 1 else ''))

        block_size_in_pixels_x, bsip_x_rem = divmod(settings['image_x'], settings['blocks_x'])
        block_size_in_pixels_y, bsip_y_rem = divmod(settings['image_y'], settings['blocks_y'])
        pixel_aligned = bsip_x_rem == 0 and bsip_y_rem == 0
        approx = '' if pixel_aligned else 'approximately '

        print('giving a block size of ' + approx + str(block_size_in_pixels_x) + ' * ' + \
            str(block_size_in_pixels_y) + ' pixels')


##############################################################################
# calculate minumum tiles required
##############################################################################

def _ctc_approximate(benchmark_results):
    """
    ctc: (c)ompute (t)ile (c)onfig

    find the distribution of blocks over nodes that will render the fastest
    using a simple heuristic

    this function is the partner of ctc_accurate(), a function which is much
    slower, but much closer to optimal in almost all cases.  it provides a
    service to _compute_block_config()

    --------------------------------------------------------------------------
    args
        benchmark_results : list of floats
            benchmark times for the nodes in the cluster
    --------------------------------------------------------------------------
    returns
        list of ints
           containing the number of blocks to allocate to each node
           that will give the quickest overall render time
    --------------------------------------------------------------------------
    """
    # allocate 1 block to the slowest node
    # for all the other nodes, allocate as many blocks as possible,
    # as long as the time to complete for each node is less than that of the slowest node
    # this is not optimal, but is quick and provides reasonable node utilisation
    slowest_benchmark_time = max(bench_time for _, bench_time in benchmark_results)
    opt = [slowest_benchmark_time // bench_time for _, bench_time in benchmark_results]
    return opt

def block_dist(num_blocks, num_nodes):
    """
    generate all the ways to distribute the given number of blocks over all the
    active nodes

    Balls (blocks), cells (nodes) and bars (marking of boundaries between
    nodes) technique from:
    Feller, W. (1968) An Introduction to Probability Theory and Its
    Applications, Volume 1, 3rd edition, London, Wiley, p. 38.

    --------------------------------------------------------------------------
    args
        num_blocks : int
            the number of nodes over which to distribute blocks
        num_nodes : int
            the number of blocks to be distributed
    --------------------------------------------------------------------------
    yields
        lists of ints
            for example, one yielded list for a 5 node cluster could be
            [1, 0, 7, 6, 3] where node 0 has 1 block allocated to it,
            node 1 has none, node 2 receives 7 blocks, node 3 gets 6
            and node 4 receives 3
    --------------------------------------------------------------------------
    """
    ntn, k = num_blocks + num_nodes - 1, num_nodes - 1
    for com in itertools.combinations(range(ntn), k):
        yield [y - x - 1 for y, x in zip(com + (ntn,), (-1,) + com)]

def _fastest_option(bench, blocks_left):
    """
    for all the ways to distribute n blocks over m nodes, find which will
    complete in the shortest time, given the benchmark times for each node

    the max() comparison is used, as the overall render time of the cluster
    is that of the node that completes last.  from all these accumulated
    results, we then look for the one with the minimum overall render time.

    --------------------------------------------------------------------------
    args
        bench : list of floats
            benchmark times for the nodes in the cluster
        blocks_left : int
            the number of blocks to distribute over the nodes
    --------------------------------------------------------------------------
    returns
        list of ints
           containing the number of blocks to allocate to each node
           that will give the quickest overall render time given
           the specific number of blocks
    --------------------------------------------------------------------------
    """
    return min(block_dist(blocks_left, len(bench)), key=lambda x: \
        max(y * z for y, z in zip(bench, x)))

def _ctc_acc_core(nti, bmt):
    """
    ctc_acc : (c)ompute (t)ile (c)onfig (acc)urate

    find the distribution of blocks over nodes that will render the fastest
    given a specific number of blocks

    run asynchronously from _ctc_accurate()

    --------------------------------------------------------------------------
    args
        nti : int
            (n)umber of (ti)les
        bmt : list of ints
            (b)ench(m)ark (t)imes
    --------------------------------------------------------------------------
    returns
        tuple (x, y)
            where:
            x = float, the estimated completion time
            y = list of ints, the number of blocks allocated to each node
    --------------------------------------------------------------------------
    """
    perf_size = [x / nti for x in bmt]
    opt_conf = _fastest_option(perf_size, nti)
    opt_time = max(x * y for x, y in zip(opt_conf, perf_size))
    return (opt_time, opt_conf)

def _ctc_accurate(benchmark_results):
    """
    ctc: (c)ompute (t)ile (c)onfig

    find the distribution of blocks over nodes that will render the fastest
    try with numbers of blocks from 1, through to 6 times the number of nodes

    this function is the partner of ctc_approximate(), and provides a service
    to _compute_block_config()

    due to the complexity of the solution (the necessary work
    increases by about 17x each time the number of nodes is incremented),
    the gains of using concurrency here are largely to make this function
    more usable on single board computers using mobile chipsets.

    --------------------------------------------------------------------------
    args
        benchmark_results : list of tuples (x, y)
            where:
            x = string, ip address
            y = int, benchmark time
            such as:
            [('10.0.0.7', 30), ('10.0.0.8', 10), ('10.0.0.17', 65)]
    --------------------------------------------------------------------------
    returns
        list of ints
            where each int is the number of blocks allocated to each node
            these are in the same node order as benchmark_results
            for example, for a returned value of [2, 6, 1]
            2 blocks would be allocated to 10.0.0.7, 6 blocks to 10.0.0.8 and
            1 block to 10.0.0.17
    --------------------------------------------------------------------------
    """
    bmt = [bench_time for _, bench_time in benchmark_results]
    num_nodes = len(benchmark_results)

    # this way of calculating block_max works well for the small numbers of
    # nodes it is practical to use this function for.  the majority of the
    # best solutions are found within the bounds using x5. using x6 is useful
    # for capturing a few outliers when the variance of the benchmarks in the
    # cluster is high
    block_max = num_nodes * 6

    try:
        num_cpu = mp.cpu_count()
    except NotImplementedError:
        num_cpu = 1

    _ctcac_pf = functools.partial(_ctc_acc_core, bmt=bmt)
    with cf.ProcessPoolExecutor(max_workers=num_cpu) as executor:
        return min(executor.map(_ctcac_pf, range(1, block_max + 1)), key=lambda x: x[0])[1]

def _prune_slow_nodes(ar_nodes, settings, bench_vals, min_blk):
    """
    remove nodes one by one, slowest first, until (1) the difference in
    performance between the slowest and fastest nodes has narrowed
    sufficiently or (2) there is only one node left

    this function should only be called for particularly small images

    the minimum number of blocks to split the image into is defined by the
    difference in performance between nodes, and this is particularly
    important for smaller images where the overhead of network file transfers
    starts to become a significant portion of the render times.

    while small images are not the focus of this script, we can avoid the
    worst performance penalties by reducing the minimum number of blocks.

    --------------------------------------------------------------------------
    args
        ar_nodes : list of RenderNode instances
            contains information about all the nodes in the cluster
        settings : image_config dictionary
            contains core information about the image to be rendered
        bench_vals : list of tuples (ip address, benchmark time)
        min_blk : int
            the minimum number of blocks required to efficiently utilise
            all available render nodes
    --------------------------------------------------------------------------
    returns
        ar_nodes, bench_vals
            mutable types amended in place, no explicit return
    --------------------------------------------------------------------------
    """
    image_pixels = settings['image_x'] * settings['image_y']
    block_size = image_pixels / min_blk
    if block_size >= settings['tile_size_x'] * settings['tile_size_y']:
        return False

    print('block size is very small: please check image size and relative node performance')

    removed = set()
    bench_vals.sort(key=lambda x: x[1])
    fastest_bench = bench_vals[0]

    while len(ar_nodes) > 1:
        slowest_bench = bench_vals[-1]
        rel_diff = slowest_bench[1] / fastest_bench[1]

        # unless the nodes are closely matched, best to remove the slowest
        if rel_diff >= 1.5:
            for i, rnode in enumerate(ar_nodes):
                if rnode.ip_address == slowest_bench[0]:
                    removed.add(slowest_bench[0])
                    del ar_nodes[i]
                    del bench_vals[-1]
                    break
        else:
            break

    if removed:
        suffix = '' if len(removed) == 1 else 's'
        print('to increase block size, the slowest node' + suffix + \
            ' will be left idle: ' + ', '.join(removed))

    return True

def _compute_block_config(avrn, benchmark_results, i_settings):
    """
    calculates (1), the mimimum number of blocks that will be needed to fairly
    distribute load over the available render nodes, given their relative
    performance and (2), their arrangement based on the dimensions of the
    image to be rendered

    even if the render setup the user has specified is going to be inefficient
    (small image and/or high variance in node performance), try to make some
    adjustments to mitigate the inefficiencies, and proceed with the render
    anyway.

    --------------------------------------------------------------------------
    args
        avrn : list of RenderNode instances
            contains information about all the nodes in the cluster
        benchmark_results : list of tuples (ip address, benchmark time)
        image_settings : instance of class RenderConfig
            contains core information about the image to be rendered
    --------------------------------------------------------------------------
    returns
        no explicit return
            the results are written to image_settings within the call to
            _block_layout()
    --------------------------------------------------------------------------
    """
    num_nodes = len(avrn)

    if num_nodes > 1:
        # choose the appropriate solver
        if num_nodes < 6:
            block_distribution = _ctc_accurate(benchmark_results)
        else:
            block_distribution = _ctc_approximate(benchmark_results)
        minimum_blocks_required = sum(block_distribution)

        if _prune_slow_nodes(avrn, i_settings, benchmark_results, minimum_blocks_required):
            # nodes have been removed, so need to recalculate
            if num_nodes < 6:
                block_distribution = _ctc_accurate(benchmark_results)
            else:
                block_distribution = _ctc_approximate(benchmark_results)
            minimum_blocks_required = sum(block_distribution)
    else:
        # there is only one node, so send the whole image to it in one piece
        minimum_blocks_required = 1

    # allow the user to influence the number of blocks the script uses
    if i_settings['blocks_user'] >= minimum_blocks_required:
        _block_layout_3(i_settings, i_settings['blocks_user'])
    else:
        # derive the best block layout
        _block_layout(i_settings, minimum_blocks_required)

    i_settings['blocks_required'] = i_settings['blocks_x'] * i_settings['blocks_y']
    i_settings['padding'] = len(str(i_settings['blocks_required']))

    # write configuration to disk, in case this script is interrupted before the render is complete
    fio.config_write(i_settings)


##############################################################################
# read, validate and store user settings
##############################################################################

def _check_setting_int(settings, name, val):
    """
    check for render settings with simple integer values

    provides a service to _read_user_options()
    """
    if val.isdigit():
        settings[name] = int(val)
    else:
        print(name + '=' + val + ' (invalid: positive integer expected)')

def _check_setting_bool(settings, name, val):
    """
    check for render settings with boolean values

    provides a service to _read_user_options()
    """
    if val.lower() == 'true':
        settings[name] = True
    elif val.lower() == 'false':
        settings[name] = False
    else:
        print(name + '=' + val + ' (invalid: boolean value True/False expected)')

def _check_setting_directory(settings, name, val):
    """
    check if the directory exists

    provides a service to _read_user_options()
    """
    if os.path.isdir(val):
        settings[name] = val
        print(name, 'set to', val)
    else:
        print(name + '=' + val + ' (invalid: directory does not exist)')

def _check_setting_backup(settings, name, val):
    """
    test to see if we can perform a backup to the target machine
    if it fails the script will report the error and then exit

    provides a service to _read_user_options()
    """
    print('>> testing backup')
    settings[name] = val
    fio.backup_render(settings, True)

def _check_setting_order(settings, name, val):
    """
    the order tiles are despatched to render nodes

    provides a service to _read_user_options()
    """
    if val in ['CENTRE', 'BOTTOM_TO_TOP']:
        settings[name] = val

def _read_user_options(render_nodes, settings):
    """
    read user set render options from a configuration file on disk
    and perform some basic validation checks

    it is possible for the user to have set the same values multiple
    times, the last value set will be the one that is used.

    ignore any line with a hash (#) *anywhere*

    use the check_setting dictionary to call a function appropriate to the
    variable name the user is trying to set this performs some basic checks to
    see the supplied value is reasonable

    --------------------------------------------------------------------------
    args
        render_nodes : list of RenderNode instances
            contains information about all the nodes in the cluster
        settings : image_config dictionary
            contains core information about the image to be rendered
    --------------------------------------------------------------------------
    returns
        render_nodes : list of RenderNode instances
            mutable type amended in place, no explicit return
        settings : image_config dictionary
            mutable type amended in place, no explicit return
    --------------------------------------------------------------------------
    """
    check_setting = {
        'image_x': _check_setting_int,
        'image_y': _check_setting_int,
        'seed': _check_setting_int,
        'frame': _check_setting_int,
        'blocks_user': _check_setting_int,
        'textures_directory': _check_setting_directory,
        'library_directory': _check_setting_directory,
        'auto_backup': _check_setting_backup,
        'render_order': _check_setting_order,
        'remove_alpha': _check_setting_bool,
        'spatial_splits': _check_setting_bool
    }

    print('>> checking user settings file')
    if not os.path.isfile(ds.FILENAME_USER_SETTINGS):
        print('user settings file', ds.FILENAME_USER_SETTINGS, 'not found: exiting')
        sys.exit()

    with open(ds.FILENAME_USER_SETTINGS) as user_settings_file:
        for line in user_settings_file:
            # expected format is variable=value
            # remove whitespace from around each part
            str_tidy = [x.strip() for x in line.rpartition('=')]
            assignment_malformed = '' in str_tidy
            line_is_a_comment = '#' in line

            if line_is_a_comment or assignment_malformed:
                continue

            # this looks like a plausible user setting, unpack into parts
            var_name, _, value = str_tidy

            if var_name == 'node':
                # user is setting a render node ip address or host name

                # check if we have just the ip address or host name, or
                # if we have something of the form: username@ip_address
                username, _, ip_hostname = value.rpartition('@')
                if ip_hostname:
                    render_nodes.append(ds.RenderNode(ip_hostname))
                    if username:
                        render_nodes[-1].username = username
            else:
                # check line for other recognised settings
                # and set them if their values are reasonable
                if check_setting.get(var_name):
                    check_setting[var_name](settings, var_name, value)


##############################################################################
# check user supplied command line arguments
##############################################################################

def check_arguments():
    """
    handle basic command line options

    command line arguments act on data files that this script creates:
    the benchmark cache (which persists across renders), and temporary files
    that store information about the progress of an ongoing distributed render

    --------------------------------------------------------------------------
    args : none
    --------------------------------------------------------------------------
    returns : none
    --------------------------------------------------------------------------
    """
    parser = argparse.ArgumentParser(description=\
        'Distributed single frame render for Blender Cycles')
    parser.add_argument( \
        '-c', '--clean', \
        action='store_true', \
        help='forces a clean start by removing the temporary files \
        associated with a previously interrupted render')
    parser.add_argument( \
        '-f', '--flush', \
        action='store_true', \
        help='flush the benchmark cache, this option implies --clean')
    args = parser.parse_args()

    if args.flush:
        fio.flush_cache()

    if args.clean or args.flush:
        fio.tidy_up_restart()


##############################################################################
# initial startup
##############################################################################

def normal_start(arn, ic_settings):
    """
    normal start

    collect all the information about the distributed render to be performed
    from the user config file FILENAME_USER_SETTINGS

    --------------------------------------------------------------------------
    args
        arn : list of RenderNode instances
            contains information about all the nodes in the cluster
        ic_settings : image_config dictionary
            contains core information about the image to be rendered
    --------------------------------------------------------------------------
    returns
        blocks_to_render : list of unique ints
            all the blocks that need be rendered
        time_taken_previous_renders : float
            as this is a normal start, and there are no previous renders,
            this value is zero
    --------------------------------------------------------------------------
    """

    print('>> normal start')

    # read user options from configuration file
    _read_user_options(arn, ic_settings)

    # exit if the image has zero pixels
    if ic_settings['image_x'] == 0 or ic_settings['image_y'] == 0:
        print('exiting,', ic_settings['image_x'], '*', ic_settings['image_y'], 'cannot be rendered')
        sys.exit()

    # make sure the user has provided at least one render node
    if not arn:
        print('exiting, no nodes specified in ' + ds.FILENAME_USER_SETTINGS)
        sys.exit()

    # remove any nodes the user may have listed more than once
    utils.no_dupes_ipu(arn)

    print('>> checking if render nodes are responsive')

    utils.remove_dead_nodes(arn)
    bench_record = bm.benchmark_new_nodes(arn)

    _compute_block_config(arn, bench_record, ic_settings)

    # contains only unique block numbers
    # but we require the ordered properties of a list later
    blocks_to_render = list(range(1, ic_settings['blocks_required'] + 1))

    # set the despatch order given by the user
    utils.despatch_order(ic_settings, blocks_to_render)

    time_taken_previous_renders = 0.0

    return blocks_to_render, time_taken_previous_renders

def restart_interrupted_render():
    """
    restart interrupted render

    --------------------------------------------------------------------------
    args : none
    --------------------------------------------------------------------------
    returns
        avail_render_nodes : list of RenderNode instances
            contains information about all the nodes in the cluster
        settings : image_config dictionary
            contains core information about the image to be rendered
        blocks_to_render : list of unique ints
            all the blocks that need be rendered
        time_taken_previous_renders : float
            the accumulated time taken by all previous parts of this render
    --------------------------------------------------------------------------
    """

    # simple restart of a previously interrupted render
    # using the settings given when the render was first started

    # restore configuration and progress
    settings = fio.config_read()
    blocks_to_render, avail_render_nodes, time_taken_previous_renders = fio.progress_read()

    # no blocks are in progress as we have restarted, so overwrite previous
    # settings
    for i in avail_render_nodes:
        i.block_in_progress = 0
        i.first_use = True

    # check that the render nodes that were alive when the render started,
    # are still alive now
    print('>> restarting previous render')
    print('>> checking if render nodes are responsive')
    utils.remove_dead_nodes(avail_render_nodes)

    # check restored progress against the blocks we have stored locally
    missing = fio.verify_blocks_present(settings, blocks_to_render)
    if missing:
        blocks_to_render.extend(missing)
        l_miss = len(missing)

        # set the despatch order given by the user
        utils.despatch_order(settings, blocks_to_render)

        word1, word2 = (' was ', ' it ') if l_miss == 1 else ('s were ', ' they ')
        print(l_miss, 'block' + word1 + 'marked as complete, but the associated block render' + \
            word1 + 'missing;' + word2 + 'will be re-rendered')

    blocks_already_rendered = settings['blocks_required'] - len(blocks_to_render)
    percent_complete = int((100 * blocks_already_rendered) / settings['blocks_required'])

    print(str(blocks_already_rendered) + ' of ' + str(settings['blocks_required']) + ' blocks (' + \
        str(percent_complete) + '%) already completed. ' + \
        str(datetime.timedelta(seconds=int(time_taken_previous_renders))) + \
            ' taken by previous block renders')

    return avail_render_nodes, settings, blocks_to_render, time_taken_previous_renders
