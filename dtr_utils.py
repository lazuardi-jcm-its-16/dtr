"""
utilities

callable functions from this file:

    block_render_filename
    despatch_order
    no_dupes_ipu
    package_filename_for_cygwin
    render_block_normal

------------------------------------------------------------------------------
Copyright 2015-2017 Alan Taylor

This file is part of dtr.

dtr is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

dtr is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with dtr.  If not, see <http://www.gnu.org/licenses/>.
------------------------------------------------------------------------------
"""

import concurrent.futures as cf # ThreadPoolExecutor
import math                     # hypot
import multiprocessing as mp    # cpu_count, Queue
import socket                   # AF_INET, SOCK_STREAM, socket
import subprocess               # PIPE, Popen
import sys                      # exit


##############################################################################
# general support functions
##############################################################################

def _axis_bounds(blocks, block_roc):
    """
    returns the single axis minimum and maximum bounds for a given block in
    the range of 0 to 1

    no attempt is made to manually align to pixel boundaries or centres,
    Blender seems happiest with this arrangement

    for example, x-axis values for block 7 would be called as
    _axis_bounds(4, 2) from render_block_normal(), where 4 is the number of
    blocks in the axis, and 2 is the block column number (starting at zero),
    and would return (0.5, 0.75)

    +---+---+---+---+
    | 5 | 6 |>7<| 8 |
    +---+---+---+---+
    | 1 | 2 | 3 | 4 |
    +---+---+---+---+
            ^   ^
            |   |
          0.5   0.75

    --------------------------------------------------------------------------
    args
        blocks : int
            the number of blocks on the axis being considered
        block_roc : int
            block (r)ow (o)r (c)olumn
            block position on the axis being considered
    --------------------------------------------------------------------------
    returns
        tuple (float, float)
            lower and upper bounds of block in one axis
    --------------------------------------------------------------------------
    """
    lower_limit = block_roc / blocks
    upper_limit = (block_roc + 1) / blocks
    return (lower_limit, upper_limit)

def render_block_normal(settings, block_number):
    """
    return bottom left (x, y) and top right (x, y) of a given block number
    where values of x and y are between 0 and 1
    assume block 0 is bottom left, and block n (max value) is top right
    compute row and column of given block number

    --------------------------------------------------------------------------
    args
        settings : image_config dictionary
            contains core information about the image to be rendered
        block_number : int
            block number to be rendered
            e.g. for a render with 8 blocks, blocks would be numbered
            {1, 2, 3, 4, 5, 6, 7, 8}
    --------------------------------------------------------------------------
    returns
        tuple (float, float, float, float)
            lower and upper bounds of the given block in both axes
    --------------------------------------------------------------------------
    """
    block_row, block_col = divmod(block_number - 1, settings['blocks_x'])
    x_min, x_max = _axis_bounds(settings['blocks_x'], block_col)
    y_min, y_max = _axis_bounds(settings['blocks_y'], block_row)

    return (x_min, y_min, x_max, y_max)

def _dist_from_centre(settings, block_number):
    """
    return the distance between the centre of the image and the
    centre of the block number given, respecting the aspect ratio of the
    image

    --------------------------------------------------------------------------
    args
        settings : image_config dictionary
            contains core information about the image to be rendered
        block_number : int
            block number to be rendered
    --------------------------------------------------------------------------
    returns
        float
            the block's distance from the centre of the image
    --------------------------------------------------------------------------
    """
    x_min, y_min, x_max, y_max = render_block_normal(settings, block_number)
    block_centre_x = (x_max + x_min) / 2.0
    block_centre_y = (y_max + y_min) / 2.0
    dxc = (0.5 - block_centre_x) * (settings['image_x'] / settings['image_y'])
    dyc = 0.5 - block_centre_y

    return math.hypot(dxc, dyc)

def no_dupes_ipu(original):
    """
    generic function that removes duplicate items from a list (i)n (p)lace;
    works for (u)n/hashable items

    specifically intended to work with a list of RenderNode instances,
    in case the user mistakenly listed ip addresses more than once
    in the configuration file

    --------------------------------------------------------------------------
    args
        original : list
    --------------------------------------------------------------------------
    returns
        original : list
            mutable type amended in place, no explicit return
    --------------------------------------------------------------------------
    """
    for j in original:
        ndup = original.count(j)
        o_ind = len(original) - 1
        while ndup > 1:
            if original[o_ind] == j:
                del original[o_ind]
                ndup -= 1
            o_ind -= 1

def package_filename_for_cygwin(node_op_sys, nix_filename):
    """
    Windows applications will expect to receive Windows filenames
    use Cygwin's cygpath() to perform the conversion

    --------------------------------------------------------------------------
    args
        node_op_sys : string
            the operating system for this render node, as given by uname
        nix_filename : string
            the standard filename, suitable for *nix type filesystems
    --------------------------------------------------------------------------
    returns
        string
            *nix or Cygwin-wrapped filename as appropriate
    --------------------------------------------------------------------------
    """
    if 'CYGWIN' not in node_op_sys:
        return nix_filename
    else:
        return '\"$(cygpath -aw \"' + nix_filename + '\")\"'

def block_render_filename(settings, block):
    """
    return a filename given the block number and seed

    --------------------------------------------------------------------------
    args
        settings : image_config dictionary
            contains core information about the image to be rendered
        block : int
            block number to be rendered
    --------------------------------------------------------------------------
    returns
        string
            filename for the rendered block image output
    --------------------------------------------------------------------------
    """
    return 'block_' + str(block).zfill(len(str(settings['blocks_required']))) + \
           '_seed_' + str(settings['seed']) + '.png'

def _node_alive(r_node):
    """
    establish whether the node is up by attempting to connect to port 22,
    as this port is typically used for ssh connections which this script
    uses heavily. a timeout period of 2 seconds is used.

    in essence we are looking to check the node is awake without having to
    handle the platform dependent nature of ping, such as differing
    return codes and command line options
    --------------------------------------------------------------------------
    args
        r_node : instance of RenderNode
            contains information about the node we are checking
    --------------------------------------------------------------------------
    returns
        bool
            True if node is up, False otherwise
    --------------------------------------------------------------------------
    """
    skt = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    skt.settimeout(2)

    try:
        skt.connect((r_node.ip_address, 22))
    except OSError:
        # connection failed
        alive = False
    else:
        # connection successful
        alive = True
    finally:
        skt.close()

    return alive

def _modern_blender_executable(node, test_blender):
    """
    detect whether the located Blender binary can be run, and whether it is
    a new enough version to be used

    The minimum Blender version deemed to be usable is 2.65 (released in 2012)
    as this release added the ability to manually set the tile size, which
    this script relies upon.
    --------------------------------------------------------------------------
    args
        node : RenderNode instance
            holds data for this render node
        test_blender : string
            command such as 'blender -v' to give verbose output
            so we can obtain the software version
    --------------------------------------------------------------------------
    returns
        bool
            True if Blender can be run, and is a new enough version
            False if Blender cannot be run, or is an old unsupported version
    --------------------------------------------------------------------------
    """
    response = _remote_command(node, test_blender)
    if 'Blender' not in response:
        return False

    b_vers = response.split()[1]
    if float(b_vers) < 2.65:
        print(node.ip_address, 'Blender version (' + b_vers.ljust(4) + \
            ') is too old to support the required feature set')
        return False

    return True

def _locate_blender(rni):
    """
    find the location of the Blender binary on the remote render node
    and perform a simple test to see if it can be executed

    --------------------------------------------------------------------------
    args
        rni : RenderNode instance
            (r)ender (n)ode (i)nstance - holds data for this render node
    --------------------------------------------------------------------------
    returns : string containing path or None (if binary not found)
    --------------------------------------------------------------------------
    """
    # see if we can find the binary with 'which'
    find_response = _remote_command(rni, 'which blender')
    if 'blender' in find_response:
        # binary found, now test to see if we can run it
        test_blender = 'blender -v'
        if _modern_blender_executable(rni, test_blender):
            return 'blender'

    # see if we can find the binary in ~
    blender_binloc = {
        'Darwin': '~/Blender/blender.app/Contents/MacOS/blender',
        'Linux': '~/Blender/blender',
        'CYGWIN' : '~/Blender/blender.exe'}
    b_loc = blender_binloc.get(rni.os, '~/Blender/blender.exe')
    find_blender = 'if [ ! -f ' + b_loc +  ' ]; then echo "MISSING"; fi'
    find_response = _remote_command(rni, find_blender)
    if 'MISSING' not in find_response:
        # binary found, now test to see if we can run it
        test_blender = b_loc + ' -v'
        if _modern_blender_executable(rni, test_blender):
            return b_loc
        else:
            return ''

    # Blender was either not found or not operational
    print(rni.ip_address + ' is up, but Blender was not found, or could not be executed')
    return ''

def _remote_command(node, execute_command):
    """
    execute command on remote node, wait for completion, then return stdout
    response

    Popen.communicate() is used here to make sure the command has fully
    completed before trying to read stdout

    --------------------------------------------------------------------------
    args
        remote_ip : string
            ip address of remote node
        execute_command : string
            command to execute
    --------------------------------------------------------------------------
    returns : string containing stdout response from command
    --------------------------------------------------------------------------
    """
    on_this_node = node.username + '@' + node.ip_address
    remcom = subprocess.Popen(['ssh', on_this_node, execute_command], shell=False, \
        stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    (stdo, _) = remcom.communicate(None, None)

    return stdo.decode('utf-8', 'ignore').rstrip()

def _check_node(node):
    """
    determine whether the node is up with a basic connection check,
    only then if we get a reply should we try to run a test command using ssh
    this should help control ssh's sometimes unpredictable behaviour when
    nodes are down

    --------------------------------------------------------------------------
    args
        node : RenderNode instance
            contains information about a single node
    --------------------------------------------------------------------------
    returns
        tuple x, y, z
            where:
            x = RenderNode instance, the node being checked
            y = boolean, whether the node is up
            z = string, Blender binary filename
    --------------------------------------------------------------------------
    """
    node_up = True
    binloc = ''

    if _node_alive(node):

        # node appears to be up and responsive (check 1 passed)
        uname_response = _remote_command(node, 'uname')
        if uname_response:

            # successfully ran a command over ssh (check 2 passed)
            node.os = 'CYGWIN' if 'CYGWIN' in uname_response else uname_response
            blender_location = _locate_blender(node)
            if blender_location:

                # Blender binary found and is operational (check 3 passed)
                binloc = blender_location
            else:
                # node is up, but appears to be misconfigured
                node_up = False
        else:
            # node appears to be misconfigured
            node_up = False
            print(node.ip_address + ' is up, but did not respond to a command sent via ssh')
    else:
        # the node may have simply gone to sleep, but it could be
        # misconfigured. as we can't tell whether it is misconfigured
        # or not, we must remove the node
        node_up = False
        print(node.ip_address + ' is down')

    return node, node_up, binloc

def remove_dead_nodes(available_nodes):
    """
    determine whether the node is up with a basic connection check,
    only then if we get a reply should we try to run a test command using ssh
    this should help control ssh's sometimes unpredictable behaviour when
    nodes are down

    with regard to the default value of max_workers for
    concurrent.futures.ThreadPoolExecutor:
    set the value of max_workers manually to make sure we get the
    latest Python behaviour when using Python 3.4

    --------------------------------------------------------------------------
    args
        available_nodes : list of RenderNode instances
            contains information about all the nodes in the cluster
    --------------------------------------------------------------------------
    returns
        available_nodes : list of RenderNode instances
            mutable type amended in place, no explicit return
    --------------------------------------------------------------------------
    """
    # run tests and process results
    try:
        num_processes = mp.cpu_count() * 5
    except NotImplementedError:
        num_processes = 5

    with cf.ThreadPoolExecutor(max_workers=num_processes) as executor:
        node_status = {executor.submit(_check_node, anode): anode for anode in available_nodes}
        for status in cf.as_completed(node_status):
            node, node_up, binloc = status.result()
            if node_up:
                node.binloc = binloc
            else:
                available_nodes.remove(node)

    # if no nodes are up, there is no point continuing
    if not available_nodes:
        print('exiting, as none of the given render nodes appear to be responsive')
        sys.exit()


##############################################################################
# set the order that blocks are despatched to be rendered
##############################################################################

def _do_bottom_to_top(ic_settings, blocks):
    """
    bottom to top, with blocks running from left to right

    provides a service to despatch_order()
    """
    blocks.sort(reverse=True)

def _do_centre(ic_settings, blocks):
    """
    from centre outwards

    provides a service to despatch_order()
    """
    blocks.sort(key=lambda x: _dist_from_centre(ic_settings, x), reverse=True)

def despatch_order(ic_settings, blocks):
    """
    set the order that blocks are despatched to be rendered

    --------------------------------------------------------------------------
    args
        ic_settings : image_config dictionary
            contains core information about the image to be rendered
        blocks : list of unique ints
            all the blocks that need be rendered
    --------------------------------------------------------------------------
    returns
        blocks : list of unique ints
            mutable type amended in place, no explicit return
    --------------------------------------------------------------------------
    """
    check_setting = {
        'CENTRE': _do_centre,
        'BOTTOM_TO_TOP': _do_bottom_to_top
    }

    check_setting[ic_settings['render_order']](ic_settings, blocks)
