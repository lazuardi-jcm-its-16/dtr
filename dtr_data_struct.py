"""
contains data structures used by the project

when this script divides up an image into parts to send to render nodes,
we will refer to those parts as 'blocks' rather than 'tiles'
to avoid confusion with Blender's own tiling system

------------------------------------------------------------------------------
Copyright 2015-2017 Alan Taylor

This file is part of dtr.

dtr is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

dtr is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with dtr.  If not, see <http://www.gnu.org/licenses/>.
------------------------------------------------------------------------------
"""

import signal                # signal


FILENAME_BENCHMARK = 'bench.blend'
FILENAME_BENCHMARK_CACHE = 'benchmark_cache.p'
FILENAME_CONFIG_BLENDER = 'render_block_'
FILENAME_FOR_RENDER = 'render.blend'
FILENAME_RESTART_CONFIG = 'restart_config.p'
FILENAME_RESTART_PROGRESS = 'restart_progress.p'
FILENAME_USER_SETTINGS = 'user_settings.conf'


class RenderNode:
    """define nodes available to render tiles"""
    def __init__(self, ip_address, username='render', os='', binloc='', mean_duration=0.0, num_blocks=0, block_time_start=0.0, block_in_progress=0, first_use=True):
        self.ip_address = ip_address                       # ip address of node
        self.username = username                           # username relating to the ip_address
        self.os = os                                       # operating system of the node is determined at runtime
        self.binloc = binloc                               # location of Blender binary
        self.mean_duration = mean_duration                 # the mean completion time of all tiles rendered by this node
        self.num_blocks = num_blocks                       # the number of blocks rendered so far
        self.block_time_start = block_time_start           # the time the current block started rendering
        self.block_in_progress = block_in_progress         # the number of the block currrently being rendered by this node
        self.first_use = first_use                         # flag to indicate whether the node has been used before (should we copy files to it)

    # using the equality test to establish whether we have duplicate nodes
    def __eq__(self, other):
        return self.ip_address == other.ip_address


class UserExit:
    """handle user interrupts gracefully"""
    interrupt = False

    def __init__(self):
        signal.signal(signal.SIGINT, self.exit_gracefully)
        signal.signal(signal.SIGTERM, self.exit_gracefully)

    def exit_gracefully(self, signum, frame):
        """set flag indicating user interrupt"""
        self.interrupt = True
