#!/usr/bin/env python3
"""
Distributed single frame render for Blender Cycles

------------------------------------------------------------------------------
Copyright 2015-2017 Alan Taylor

This file is part of dtr.

dtr is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

dtr is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with dtr.  If not, see <http://www.gnu.org/licenses/>.
------------------------------------------------------------------------------
"""

import datetime              # timedelta
import math                  # exp
import multiprocessing as mp # Process
import os                    # path.isfile
import subprocess            # call
import sys                   # exit, version
import time                  # sleep, time

import dtr_analysis as ana   # display_basic_render_info
import dtr_benchmark as bm   # display_bench_cache
import dtr_data_struct as ds # FILENAME_CONFIG_BLENDER
                             # FILENAME_FOR_RENDER
                             # FILENAME_RESTART_CONFIG
                             # FILENAME_RESTART_PROGRESS
import dtr_file_io as fio    # backup_render, create_final_image_from_blocks,
                             # distribute_files_to_node, progress_write,
                             # render_data_write
import dtr_init as init      # block_dist, check_arguments,
                             # display_render_details, normal_start,
                             # restart_interrupted_render
import dtr_utils as utils    # block_render_filename,
                             # package_filename_for_cygwin


##############################################################################
# support
##############################################################################

def asynchronous_render(settings, render_jobs, node, rn_index, rt_block):
    """
    issue a block to a node for rendering

    --------------------------------------------------------------------------
    args
        settings : image_config dictionary
            contains core information about the image to be rendered
        render_jobs : list of returns from multiprocessing.Process
            later on we will need to poll this list to determine if the node
            has finished rendering
        node : instance of RenderNode
            contains information about the node we will send a block to
        rn_index : int
            (r)ender (node) index
            the index of 'node' above in its parent list
        rt_block : int
           the number of the block to be rendered
    --------------------------------------------------------------------------
    returns
        render_jobs : list of returns from multiprocessing.Process
            mutable type amended in place, no explicit return
    --------------------------------------------------------------------------
    """
    node.block_time_start = time.time()
    node.block_in_progress = rt_block

    ar_proc = mp.Process(target=despatch_block, args=(settings, node))
    ar_proc.start()
    render_jobs[rn_index] = ar_proc
    print('block ' + str(rt_block).rjust(settings['padding']) + \
        ' issued to node ' + node.ip_address)

def record_running_mean(rnode):
    """
    running mean calculation

    recalculate mean block render time for a node, given a new time from a
    newly completed block

    --------------------------------------------------------------------------
    args
        rnode : RenderNode instance
            contains information about a node in the cluster
    --------------------------------------------------------------------------
    returns
        rnode : RenderNode instance
            mutable type amended in place, no explicit return
    --------------------------------------------------------------------------
    """
    new_value = time.time() - rnode.block_time_start
    rnode.mean_duration = \
        (rnode.mean_duration * (rnode.num_blocks - 1) + new_value) / rnode.num_blocks

def get_block_to_be_rendered(blocks):
    """
    return a block number that can be rendered by a remote render node
    from the set of all available blocks yet to be rendered
    return 0 if there are no blocks left to distribute

    --------------------------------------------------------------------------
    args
        blocks : list of unique ints
           contains the numbers of all the blocks yet to be issued
           for rendering
    --------------------------------------------------------------------------
    returns
        int
            block number to be rendered, or 0 if the set is empty
    --------------------------------------------------------------------------
    """
    if blocks:
        return blocks.pop()
    else:
        return 0


##############################################################################
# give a block to this node or not
##############################################################################

def option_time_to_complete(trd, etui, block_distribution):
    """
    given a distibution of blocks over nodes, estimate the time needed for the
    cluster of render nodes to complete rendering it

    in this example scenario, we have 6 blocks yet to begin rendering, and
    4 nodes that we can potentially issue them to:

    node ->                   0     1     2     3
    (x) trd                = [14.9, 54.1, 43.6, 16.9]    mean block render times
    (y) block_distribution = [ 3,    0,    1,    2  ]    3 blocks to node 0, 0 blocks to node 1..
    (z) etui               = [ 1.1,  0.4,  0.0,  9.8]    node 2 is idle, node 0 available next
    option_times_per_node  = [45.8,  0.4, 43.6, 43.6]    node 1 is too slow to be given more work

    therefore the total render time for this block distribution is
    45.8 seconds, which is the highest value in option_times_per_node.
    this value is returned by the function

    --------------------------------------------------------------------------
    args
        trd : list of floats
            (t)ile (r)ender (d)uration
            a list containing the mean block render time for each node
        etui : list of floats
            (e)stimated (t)ime (u)ntil (i)dle
            a list containing the estimated time for each node to complete
            its current block
        block_distribution : list of ints
            contains information about all the nodes in the cluster
    --------------------------------------------------------------------------
    returns
        float
            the estimated time for the cluster to complete rendering the
            distribution of blocks over nodes given
    --------------------------------------------------------------------------
    """
    return max(x * y + z for x, y, z in zip(trd, block_distribution, etui))

def gbttn_approximate(a_render_nodes, blocks_left, node):
    """
    (g)ive (b)lock (t)o (t)his (node) approximate method

    try to rule out giving a block to an idle slow node, if doing so would
    prolong the time to complete the render

    this function is simple, fast and provides a result which is reasonable,
    though not optimal in many circumstances. it is the partner of
    gbttn_accurate() which is much slower, but much closer to optimal.
    it provides a service to give_block_to_this_node()

    --------------------------------------------------------------------------
    args
        a_render_nodes : list of RenderNode instances
            contains information about all the nodes in the cluster
        blocks_left : int
            the number of blocks left to render
        node : string
            the node to check (ip address in string form)
    --------------------------------------------------------------------------
    returns
        bool
            true if we should give a block to this node, otherwise false
    --------------------------------------------------------------------------
    """
    # collate timing data
    timing = []
    time_now = time.time()
    for i in a_render_nodes:
        start_time = i.block_time_start if i.block_in_progress else time_now
        time_at_completion = i.mean_duration + start_time
        timing.append([i.ip_address, time_at_completion, i.mean_duration])

    # refuse if giving a block to the idle node would result in a longer
    # overall render time
    first_node_to_complete_ip, _, first_node_to_complete_mean = min(timing, key=lambda x: x[1])
    another_node_is_faster = first_node_to_complete_ip != node
    mean_for_this_node = [x for x in timing if x[0] == node][0][2]
    close_to_the_end = (mean_for_this_node / first_node_to_complete_mean) > blocks_left

    return False if another_node_is_faster and close_to_the_end else True

def gbttn_accurate(a_render_nodes, blocks_left, number_of_nodes, node):
    """
    (g)ive (b)lock (t)o (t)his (node) accurate method

    try to rule out giving a block to an idle slow node, if doing so would
    prolong the time to complete the render

    this function is fairly close to optimal in most circumstances, and
    performs an exhaustive search of the given number blocks over the nodes
    in the cluster to find the distribution that will complete in the
    fastest time. if the node we have been given does not feature in that
    distribution, then return False (that node will not be given a block).
    this takes into consideration nodes that are in the middle of rendering
    blocks, and estimates their completion time.

    it is the partner of gbttn_approximate() which is much faster, but
    less accurate. it provides a service to give_block_to_this_node()

    --------------------------------------------------------------------------
    args
        a_render_nodes : list of RenderNode instances
            contains information about all the nodes in the cluster
        blocks_left : int
            the number of blocks left to render
        number_of_nodes : int
            the number of nodes in the cluster
        node : string
            the node to check (ip address in string form)
    --------------------------------------------------------------------------
    returns
        bool
            true if we should give a block to this node, otherwise false
    --------------------------------------------------------------------------
    """
    # estimate how long nodes will be rendering their current blocks
    estimated_time_until_idle = [x.mean_duration + x.block_time_start - time.time() \
        if x.block_in_progress else 0.0 for x in a_render_nodes]

    # work out which distribution of blocks over nodes gives the shortest
    # overall render time
    block_render_duration = [x.mean_duration for x in a_render_nodes]
    fastest_opt = min(init.block_dist(blocks_left, number_of_nodes), key=lambda x: \
        option_time_to_complete(block_render_duration, estimated_time_until_idle, x))

    # if this node was allocated at least one block in the fastest option
    # then give it a block to render
    nodes = (x.ip_address for x in a_render_nodes)
    return any(n == node and b > 0 for n, b in zip(nodes, fastest_opt))

def give_block_to_this_node(render_nodes, this_node, tst_blocks):
    """
    towards the end of the render, we want to avoid giving a block to an idle
    slow node, if doing so would prolong the time to complete the render

    decide whether to give the specified (now idle) node a new block or not

    --------------------------------------------------------------------------
    args
        render_nodes : list of RenderNode instances
            contains information about all the nodes in the cluster
        this_node : RenderNode instance
            contains information about the node we are checking
        tst_blocks : list of unique ints
            contains blocks that have either yet to be issued for rendering
            or, have been issued and are still rendering
    --------------------------------------------------------------------------
    returns
        bool
            true if we should give a block to this node, otherwise false
    --------------------------------------------------------------------------
    """
    number_of_nodes = len(render_nodes)
    all_mean_values_exist = all(x.mean_duration > 0.0 for x in render_nodes)

    if all_mean_values_exist and number_of_nodes > 1:
        number_of_blocks_left_to_render = len(tst_blocks) + 1

        if (number_of_blocks_left_to_render * math.exp(number_of_nodes)) < 1650:
            # solution complexity is low, use slow and accurate method
            give_block_to_node = gbttn_accurate(render_nodes, \
                number_of_blocks_left_to_render, number_of_nodes, this_node.ip_address)
        else:
            # solution complexity is high, use the fast and approximate method
            give_block_to_node = gbttn_approximate(render_nodes, \
                number_of_blocks_left_to_render, this_node.ip_address)

        return give_block_to_node
    else:
        return True


##############################################################################
# actions to be asynchronously run on remote nodes using multiprocessing
##############################################################################

def despatch_block(settings, node):
    """
    render a block on a remote node

    run asynchronously using multiprocessing.Process from
    asynchronous_render()

    --------------------------------------------------------------------------
    args
        settings : image_config dictionary
            contains core information about the image to be rendered
        node : RenderNode instance
            contains information about the node we are checking
    --------------------------------------------------------------------------
    returns : none
    --------------------------------------------------------------------------
    """
    on_this_node = node.username + '@' + node.ip_address
    block_number_padded = str(node.block_in_progress).zfill(settings['padding'])

    filename_render = utils.package_filename_for_cygwin(node.os, \
        ds.FILENAME_FOR_RENDER)
    filename_python = utils.package_filename_for_cygwin(node.os, \
        ds.FILENAME_CONFIG_BLENDER + block_number_padded + '.py')

    render_the_block = node.binloc + ' -b ' + filename_render + \
        ' --python ' + filename_python

    # need to use subprocess.call here (blocking necessary)
    subprocess.call(['ssh', on_this_node, render_the_block], \
        stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)


##############################################################################
# file i/o
##############################################################################

def collect_block_from_node(settings, node):
    """
    return true if the file was read with no errors

    scp options:
    -q    quiet mode
    --------------------------------------------------------------------------
    args
        settings : image_config dictionary
            contains core information about the image to be rendered
        node : instance of RenderNode
            contains information about the node we are checking
    --------------------------------------------------------------------------
    returns
        bool
            true if the block could be retrieved, false otherwise
    --------------------------------------------------------------------------
    """
    remote_filename = node.username + '@' + node.ip_address + ':' + \
        utils.block_render_filename(settings, node.block_in_progress)

    resp = subprocess.call(['scp', '-q', remote_filename, '.'], \
        stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

    return resp == 0

def collected_block_admin(arn, rnode, bfr, acc_tim):
    """
    perform some administration tasks after collecting a newly completed block
    from a node

    --------------------------------------------------------------------------
    args
        arn : list of RenderNode instances
            contains information about all the nodes in the cluster
        rnode : RenderNode instance
            contains information about the node we just collected a block from
        bfr : set of int
            (s)et (o)f (b)locks (f)or (r)estart
            contains all the blocks that have yet to be issued to a node for
            rendering
        acc_time : float
            (acc)umulated (time) spent rendering, including any time spent on
            previous partially completed parts of this render
    --------------------------------------------------------------------------
    returns : none
    --------------------------------------------------------------------------
    """
    record_running_mean(rnode)
    bfr.remove(rnode.block_in_progress)
    rnode.block_in_progress = 0
    fio.progress_write((bfr, arn, acc_tim))


##############################################################################
# startup
##############################################################################

def despatch_initial_blocks(render_nodes, settings, tr_jobs, blocks_to_render):
    """
    handle issuing each node with its first block to render

    --------------------------------------------------------------------------
    args
        render_nodes : list of RenderNode instances
            contains information about all the nodes in the cluster
        settings : image_config dictionary
            contains core information about the image to be rendered
        tr_jobs : list of returns from multiprocessing.Process
            later on we will need to poll this list to determine if the node
            has finished rendering
        blocks_to_render : list of unique ints
            all the blocks that need be rendered
    --------------------------------------------------------------------------
    returns : none
    --------------------------------------------------------------------------
    """
    for i in render_nodes:
        render_block = get_block_to_be_rendered(blocks_to_render)
        if render_block:
            tr_jobs.append(None)
            if give_block_to_this_node(render_nodes, i, blocks_to_render):
                fio.distribute_files_to_node(settings, i, render_block)
                asynchronous_render(settings, tr_jobs, i, render_nodes.index(i), render_block)
            else:
                blocks_to_render.append(render_block)
        else:
            # we have fewer blocks than render nodes
            render_nodes.remove(i)

def check_python_version():
    """
    simple check that the version of Python running this script is
    3.4 or later, and exit if it is older

    --------------------------------------------------------------------------
    args : none
    --------------------------------------------------------------------------
    returns : none
    --------------------------------------------------------------------------
    """
    if sys.version_info < (3, 4):
        py_vers = sys.version.partition(' ')[0]
        print('this script requires Python 3.4 or later (' + py_vers + ' detected), exiting')
        sys.exit()

def main():
    """
    distributed render
    """
    script_start = time.time()

    # basic configuration data for image render, the options that may be
    # safely set by the user can be changed in file dtr_user_settings.txt
    image_config = {
        # USER SETTINGS
        'image_x': 3840,              # image size, x axis
        'image_y': 2160,              # image size, y axis
        'seed': 0,                    # Blender cycles noise seed
        'frame': 1,                   # frame number to be rendered
        'blocks_user': 0,             # give the user the ability to raise the number of blocks
        'textures_directory': '',     # textures associated with the render file
        'library_directory': '',      # library blender files associated with the render file
        'auto_backup': '',            # back up in-progress render to another machine using rsync
        'remove_alpha': False,        # remove alpha channel from final image
        'render_order': 'CENTRE',     # the order the blocks are rendered in
        'spatial_splits': False,      # use spatial splits: longer build time, faster render

        # RESERVED SETTINGS
        'blocks_x': -1,               # number of blocks, x axis
        'blocks_y': -1,               # number of blocks, y axis
        'blocks_required': -1,        # total number of blocks required (blocks_x * blocks_y)
        'padding': -1,                # size of block numbers field
        'tile_size_x': 16,            # x axis size of Blender tile in pixels
        'tile_size_y': 16,            # y axis size of Blender tile in pixels
        'min_tiles_per_block': 16,    # n tiles per block, tile = tile_size_x * tile_size_y pixels
        'max_threads': 8,             # = number of cores (for cpus without SMT),
                                      #     = number of cores * 2 (for cpus with SMT)
                                      #     the value used should be the highest obtained
                                      #     from all the render nodes used
        'max_blocks': 1024            # when the image to be rendered is very large,
                                      #     limit the number of blocks to something sane
    }

    # check any command line arguments supplied by the user
    init.check_arguments()

    # check if we are restarting from an earlier interrupted render
    if os.path.isfile(ds.FILENAME_RESTART_CONFIG) and os.path.isfile(ds.FILENAME_RESTART_PROGRESS):
        available_render_nodes, image_config, blocks_to_render, \
            time_taken_by_previous_renders = init.restart_interrupted_render()
    else:
        available_render_nodes = []
        blocks_to_render, time_taken_by_previous_renders = \
            init.normal_start(available_render_nodes, image_config)

    # let the user know which nodes are active, and what their performance is
    bm.display_bench_cache(available_render_nodes)

    ##########################################################################
    # render
    ##########################################################################

    init.display_render_details(available_render_nodes, image_config)

    print('>> rendering')

    # startup
    jobs = []
    blocks_for_restart = list(blocks_to_render)
    despatch_initial_blocks(available_render_nodes, image_config, jobs, blocks_to_render)

    # main loop: collect finished blocks and despatch new ones as required,
    # until all work is complete
    user_action = ds.UserExit()
    while blocks_for_restart and not user_action.interrupt:
        for ind, rrn in enumerate(available_render_nodes):

            if rrn.block_in_progress and not jobs[ind].is_alive():
                # the node has finished rendering and is now idle,
                # collect the file containing the rendered block
                block_num_padded = str(rrn.block_in_progress).rjust(image_config['padding'])
                if collect_block_from_node(image_config, rrn):
                    rrn.num_blocks += 1
                    print('block ' + block_num_padded + ' retrieved from ' + rrn.ip_address)

                    acc_time = time_taken_by_previous_renders + time.time() - script_start
                    collected_block_admin(available_render_nodes, rrn, blocks_for_restart, acc_time)

                    # backup render directory if user has requested it
                    fio.backup_render(image_config)
                else:
                    print('block ' + block_num_padded + ' could not be retrieved from ' + \
                        rrn.ip_address + ' and will rendered again')
                    blocks_to_render.append(rrn.block_in_progress)
                    rrn.block_in_progress = 0

                # send another block to render if there is one available
                render_this_block = get_block_to_be_rendered(blocks_to_render)
                if render_this_block:
                    if give_block_to_this_node(available_render_nodes, rrn, blocks_to_render):
                        fio.distribute_files_to_node(image_config, rrn, render_this_block)
                        asynchronous_render(image_config, jobs, rrn, ind, render_this_block)
                    else:
                        blocks_to_render.append(render_this_block)
                fio.progress_write((blocks_for_restart, available_render_nodes, \
                    time_taken_by_previous_renders + time.time() - script_start))

        # sleep for a short while before polling again
        time.sleep(2)

    if user_action.interrupt:
        print('\nexiting: blender instances may still be running on render nodes')
        sys.exit()

    fio.create_final_image_from_blocks(image_config)
    fio.backup_render(image_config)
    fio.tidy_up_temporary_files(available_render_nodes)
    ana.display_basic_render_info(available_render_nodes, script_start, \
        time.time(), time_taken_by_previous_renders)


##############################################################################
if __name__ == '__main__':

    # exit if the version of Python running this script is too old
    check_python_version()

    # proceed with distributed render
    main()
