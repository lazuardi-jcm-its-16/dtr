"""
benchmark render nodes

callable functions from this file:

    benchmark_new_nodes
    display_bench_cache

------------------------------------------------------------------------------
Copyright 2015-2017 Alan Taylor

This file is part of dtr.

dtr is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

dtr is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with dtr.  If not, see <http://www.gnu.org/licenses/>.
------------------------------------------------------------------------------
"""

import concurrent.futures as cf # ProcessPoolExecutor
import datetime                 # timedelta
import hashlib                  # sha1
import multiprocessing as mp    # cpu_count, Queue
import os                       # path.isdir, path.isfile
import subprocess               # call
import sys                      # exit
import time                     # time

import dtr_data_struct as ds    # FILENAME_BENCHMARK, FILENAME_BENCHMARK_CACHE
import dtr_file_io as fio       # backup_render, benchmark_cache_read, benchmark_cache_write
import dtr_utils as utils       # package_filename_for_cygwin


##############################################################################
# benchmark render nodes
##############################################################################

def _bench_info(ntb, ips):
    """
    give the user information about the number of nodes that will be
    benchmarked

    --------------------------------------------------------------------------
    args
        ntb : set of ip addresses in string form
            (n)odes (t)o be (b)enchmarked
        ips : set of ip addresses in string form
            the (ip) addresse(s) of all the nodes in the cluster
    --------------------------------------------------------------------------
    returns : none
    --------------------------------------------------------------------------
    """
    l_ntb = len(ntb)
    l_ips = len(ips)

    if l_ips == 1:
        print('running benchmark on 1 node')
    elif l_ntb == 1:
        print('running benchmark on', l_ntb, 'of', l_ips, 'nodes')
    else:
        print('running benchmarks on', l_ntb, 'of', l_ips, 'nodes')

def _nodes_to_assess(node_ips_specified):
    """
    work out which nodes need to be benchmarked, taking into consideration
    the existence of a benchmark cache, and a benchmark Blender file

    --------------------------------------------------------------------------
    args
        node_ips_specified : set of ip addresses in string form
    --------------------------------------------------------------------------
    returns
        nodes_to_benchmark : set of ip addresses in string form
        hash_cached : string
            the sha1 hash of the FILENAME_BENCHMARK file used to
            create the benchmark results, read from the benchmark cache
        bench_cached : list of tuples (node ip address, benchmark time)
            read from the benchmark cache
    --------------------------------------------------------------------------
    """
    cache_present = os.path.isfile(ds.FILENAME_BENCHMARK_CACHE)
    bench_present = os.path.isfile(ds.FILENAME_BENCHMARK)
    cache_missing = not cache_present
    bench_missing = not bench_present

    if cache_missing and bench_missing:
        # cannot continue
        print('benchmark cache and Blender benchmark file are both missing: exiting')
        sys.exit()

    elif cache_missing and bench_present:
        # benchmark all the nodes given
        nodes_to_benchmark = set(node_ips_specified)
        hash_cached = hashlib.sha1(open(ds.FILENAME_BENCHMARK, 'rb').read()).hexdigest()
        bench_cached = []

    elif cache_present and bench_missing:
        # can only continue if the cache contains all the nodes we have
        # been given
        nodes_to_benchmark = set(node_ips_specified)
        hash_cached, bench_cached = fio.benchmark_cache_read()
        nodes_in_cache = {i[0] for i in bench_cached}
        nodes_to_benchmark -= nodes_in_cache

        if nodes_to_benchmark:
            print('there is no Blender benchmark file with which to test uncached nodes: exiting')
            sys.exit()

    elif cache_present and bench_present:
        # benchmark only the nodes not found in the cache
        nodes_to_benchmark = set(node_ips_specified)
        hash_cached, bench_cached = fio.benchmark_cache_read()
        nodes_in_cache = {i[0] for i in bench_cached}
        hash_current = hashlib.sha1(open(ds.FILENAME_BENCHMARK, 'rb').read()).hexdigest()

        if hash_cached == hash_current:
            nodes_to_benchmark -= nodes_in_cache
        else:
            print(ds.FILENAME_BENCHMARK, 'has changed, cache invalidated')
            hash_cached = hash_current
            del bench_cached[:]

    return nodes_to_benchmark, hash_cached, bench_cached

def benchmark_new_nodes(nodes_specified):
    """
    run benchmarks asynchronously on all nodes for which we do not have
    results cached, update the benchmark cache with the new results, and
    return benchmarks for all the nodes in the cluster

    with regard to the default value of max_workers for
    concurrent.futures.ThreadPoolExecutor:
    set the value of max_workers manually to make sure we get the
    latest behaviour when using Python 3.4

    --------------------------------------------------------------------------
    args
        nodes_specified : list of RenderNode instances
            contains information about all the nodes in the cluster
    --------------------------------------------------------------------------
    returns
        list of tuples (node ip address, benchmark time)
            this contains benchmark results for all the nodes given in
            nodes_specified
    --------------------------------------------------------------------------
    """

    print('>> benchmarking')

    # determine which nodes we need to benchmark
    ips_specified = {i.ip_address for i in nodes_specified}
    bench_nodes, bench_hash, bench_res = _nodes_to_assess(ips_specified)

    if bench_nodes:
        # benchmarking may take some time, so give the user an idea of how
        # many nodes are involved
        _bench_info(bench_nodes, ips_specified)

        # prep list of RenderNode instances for nodes to be benchmarked
        nodes = []
        for node_ip in bench_nodes:
            nodes.append(next(x for x in nodes_specified if x.ip_address == node_ip))

        # run benchmarks and update cache with results
        try:
            num_processes = mp.cpu_count() * 5
        except NotImplementedError:
            num_processes = 5

        with cf.ThreadPoolExecutor(max_workers=num_processes) as executor:
            bench_status = {executor.submit(_benchmark_render_node, node): node for node in nodes}
            for status in cf.as_completed(bench_status):
                bench_res.append(status.result())

        # write cache to disk
        fio.benchmark_cache_write((bench_hash, bench_res))
        print('benchmark cache updated')
    else:
        print("using cached benchmarks for all nodes")

    return [x for x in bench_res if x[0] in ips_specified]

def _benchmark_render_node(rni):
    """
    run benchmark on given node, completion time is returned using the queue
    'results'

    run asynchronously from benchmark_new_nodes()

    rsync options used:
    -a    archive mode
    -c    compare files based on checksum
    -q    quiet

    --------------------------------------------------------------------------
    args
        rni : RenderNode instance
            contains information about the node we are checking
    --------------------------------------------------------------------------
    returns
        tuple x, y
            where:
            x = string, ip address
            y = int, time difference in seconds
    --------------------------------------------------------------------------
    """
    # transfer benchmark Blender file to remote node
    on_this_node = rni.username + '@' + rni.ip_address
    remote_filename = on_this_node + ':~'
    subprocess.call(['rsync', '-acq', ds.FILENAME_BENCHMARK, remote_filename], \
        stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

    filename = utils.package_filename_for_cygwin(rni.os, ds.FILENAME_BENCHMARK)
    render_the_benchmark = rni.binloc + ' -b ' + filename + ' -f 1'
    start = time.time()

    # need to use subprocess.call here (blocking necessary)
    subprocess.call(['ssh', on_this_node, render_the_benchmark], \
        stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

    time_diff = int(time.time() - start)
    return rni.ip_address, time_diff

def display_bench_cache(nodes_specified):
    """
    display contents of benchmark cache

    --------------------------------------------------------------------------
    args
        nodes_specified : list of RenderNode instances
            contains information about all the nodes in the cluster
    --------------------------------------------------------------------------
    returns : none
    --------------------------------------------------------------------------
    """
    cache_present = os.path.isfile(ds.FILENAME_BENCHMARK_CACHE)
    bench_present = os.path.isfile(ds.FILENAME_BENCHMARK)
    cache_missing = not cache_present
    bench_missing = not bench_present

    if cache_missing:
        print('benchmark cache does not exist')

    elif cache_present and bench_missing:
        print('benchmark cache exists, but its validity cannot be determined')
        _, bench_cached = fio.benchmark_cache_read()

        ips_specified = {i.ip_address for i in nodes_specified}
        mx_ip_len = max(len(x[0]) for x in bench_cached)
        for i in bench_cached:
            in_use = 'active' if i[0] in ips_specified else ''
            print('cached: node', i[0].rjust(mx_ip_len), 'benchmark time', \
                str(datetime.timedelta(seconds=int(i[1]))), in_use)

    elif cache_present and bench_present:
        hash_cached, bench_cached = fio.benchmark_cache_read()
        hash_current = hashlib.sha1(open(ds.FILENAME_BENCHMARK, 'rb').read()).hexdigest()

        if hash_cached != hash_current:
            print('benchmark cache exists, but is invalid')

        ips_specified = {i.ip_address for i in nodes_specified}
        mx_ip_len = max(len(x[0]) for x in bench_cached)
        for i in bench_cached:
            in_use = 'active' if i[0] in ips_specified else ''
            print('cached: node', i[0].rjust(mx_ip_len), 'benchmark time', \
                str(datetime.timedelta(seconds=i[1])), in_use)
