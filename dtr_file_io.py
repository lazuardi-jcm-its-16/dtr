"""
all functions involved with file i/o

callable functions from this file:

    backup_render
    benchmark_cache_read
    benchmark_cache_write
    config_read
    config_write
    distribute_files_to_node
    flush_cache
    progress_read
    progress_write
    tidy_up_restart
    tidy_up_temporary_files
    verify_blocks_present

------------------------------------------------------------------------------
Copyright 2015-2017 Alan Taylor

This file is part of dtr.

dtr is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

dtr is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with dtr.  If not, see <http://www.gnu.org/licenses/>.
------------------------------------------------------------------------------
"""

import concurrent.futures as cf # ThreadPoolExecutor
import functools                # partial
import multiprocessing as mp    # cpu_count
import os                       # path.getsize, path.isdir
import pickle                   # dump, load
import subprocess               # call
import sys                      # exit

import PIL.Image                # alpha_composite, open

import dtr_data_struct as ds    # FILENAME_BENCHMARK_CACHE,
                                # FILENAME_CONFIG_BLENDER,
                                # FILENAME_RESTART_CONFIG,
                                # FILENAME_RESTART_PROGRESS
import dtr_utils as utils       # render_block_normal, block_render_filename


##############################################################################
# manage temporary files used during the render
##############################################################################

def _data_write(data, filename):
    """
    store data to file for later retrieval

    this function is internal to this module, and is not called directly
    see partial function wrappers below (which are callable)

    --------------------------------------------------------------------------
    args
        data : datatype, or datatypes wrapped in a tuple
        filename : string
    --------------------------------------------------------------------------
    returns : none
    --------------------------------------------------------------------------
    """
    with open(filename, 'wb') as outfile:
        # use data stream format protocol 4, for Python 3.4 or later
        pickle.dump(data, outfile, 4)

# wrappers for _data_write
benchmark_cache_write = functools.partial(_data_write, filename=ds.FILENAME_BENCHMARK_CACHE)
config_write = functools.partial(_data_write, filename=ds.FILENAME_RESTART_CONFIG)
progress_write = functools.partial(_data_write, filename=ds.FILENAME_RESTART_PROGRESS)

def _data_read(filename):
    """
    retrieve previously stored data from file

    this function is internal to this module, and is not called directly
    see partial function wrappers below (which are callable)

    --------------------------------------------------------------------------
    args
        filename : string
    --------------------------------------------------------------------------
    returns
        restored_progress : tuple (x, y, z)
            where:
            x = list of ints,
            y = list of RenderNode instances,
            z = float)
            set of blocks for restart, available render nodes, time taken by
            previous renders
    --------------------------------------------------------------------------
    """
    with open(filename, 'rb') as infile:
        restored_progress = pickle.load(infile)

    return restored_progress

# wrappers for _data_read
benchmark_cache_read = functools.partial(_data_read, filename=ds.FILENAME_BENCHMARK_CACHE)
config_read = functools.partial(_data_read, filename=ds.FILENAME_RESTART_CONFIG)
progress_read = functools.partial(_data_read, filename=ds.FILENAME_RESTART_PROGRESS)

def flush_cache():
    """
    delete the benchmark cache

    --------------------------------------------------------------------------
    args : none
    --------------------------------------------------------------------------
    returns : none
    --------------------------------------------------------------------------
    """
    delete_files = ['rm', '-f', ds.FILENAME_BENCHMARK_CACHE]

    # do not check the response code
    subprocess.call(delete_files, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

def tidy_up_restart():
    """
    on the local machine, remove temporary files associated with a
    successfully completed, or previously interrupted render

    --------------------------------------------------------------------------
    args : none
    --------------------------------------------------------------------------
    returns : none
    --------------------------------------------------------------------------
    """
    delete_files = ' '.join([\
        'rm -f', \
        'block_*_seed_*.png', \
        ds.FILENAME_CONFIG_BLENDER + '*.py', \
        ds.FILENAME_RESTART_CONFIG, \
        ds.FILENAME_RESTART_PROGRESS])

    # the shell is needed to parse the wildcards
    subprocess.call(delete_files, shell=True, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

def _tidy_files(node):
    """
    tidy up files on a remote node

    as users may be performing multiple renders of the same Blender file
    we should avoid the temptation to remove the Blender file and any
    associated textures or library files, in the interest of limiting wear
    on flash based media.  so just remove python configuration files,
    rendered blocks, and image renders from benchmarking

    run asynchronously from tidy_up_temporary_files()

    --------------------------------------------------------------------------
    args
        node : instance of RenderNode
            contains information about the node we will send a block to
    --------------------------------------------------------------------------
    returns
        tuple x, y
        where:
        x = int, return code from command execution
        y = string, ip address on which the command was executed
    --------------------------------------------------------------------------
    """
    on_this_node = node.username + '@' + node.ip_address
    delete_files = ' '.join([\
        'rm -f', \
        'block_*_seed_*.png', \
        '0001.png', \
        ds.FILENAME_CONFIG_BLENDER + '*.py'])

    # subprocess.call blocks until complete
    resp = subprocess.call(['ssh', on_this_node, delete_files], \
        stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

    return resp, node.ip_address

def tidy_up_temporary_files(render_nodes):
    """
    remove temporary files on local machine and remote nodes

    with regard to the default value of max_workers for
    concurrent.futures.ThreadPoolExecutor:
    set the value of max_workers manually to make sure we get the
    latest behaviour when using Python 3.4

    --------------------------------------------------------------------------
    args
        render_nodes : list of RenderNode instances
            contains information about all the nodes in the cluster
    --------------------------------------------------------------------------
    returns : none
    --------------------------------------------------------------------------
    """
    print('>> tidying up')

    # tidy up remote nodes
    try:
        num_processes = mp.cpu_count() * 5
    except NotImplementedError:
        num_processes = 5

    with cf.ThreadPoolExecutor(max_workers=num_processes) as executor:
        delete_status = {executor.submit(_tidy_files, node): node for node in render_nodes}
        for status in cf.as_completed(delete_status):
            tidy_failed, node_ip = status.result()
            if tidy_failed:
                print('problem deleting temporary files on', node_ip)

    # tidy up locally
    tidy_up_restart()

    print('>> finished')

def verify_blocks_present(settings, blocks_yet_to_render):
    """
    check we have block renders on disk for each block marked as already
    completed return a set of any missing blocks that need to be rendered again

    --------------------------------------------------------------------------
    args
        settings : image_config dictionary
            contains core information about the image to be rendered
        blocks_yet_to_render : list of unique ints
            contains blocks that have either yet to be issued for rendering
            or, have been issued and are still rendering
    --------------------------------------------------------------------------
    returns
        missing_blocks : set of ints
            a set of blocks that need to be rendered again
    --------------------------------------------------------------------------
    """
    set_of_all_blocks = set(range(1, settings['blocks_required'] + 1))
    set_of_rendered_blocks = set_of_all_blocks - set(blocks_yet_to_render)

    missing_blocks = set()
    for block in set_of_rendered_blocks:
        missing_or_empty = False

        try:
            filesize = os.path.getsize(utils.block_render_filename(settings, block))
        except OSError:
            missing_or_empty = True
        else:
            if filesize == 0:
                missing_or_empty = True

        if missing_or_empty:
            missing_blocks.add(block)

    return missing_blocks

def create_final_image_from_blocks(settings):
    """
    stitch the rendered blocks together to form the final image

    --------------------------------------------------------------------------
    args
        settings : image_config dictionary
            contains core information about the image to be rendered
    --------------------------------------------------------------------------
    returns : none
    --------------------------------------------------------------------------
    """
    print('>> compositing')

    # stitch tiles together
    tmp = PIL.Image.open(utils.block_render_filename(settings, 1))
    if settings['blocks_required'] > 1:
        for i in range(2, settings['blocks_required'] + 1):
            image = PIL.Image.open(utils.block_render_filename(settings, i))
            tmp = PIL.Image.alpha_composite(tmp, image)

    # discard alpha channel if requested
    if settings['remove_alpha']:
        tmp = tmp.convert("RGB")

    # save final image
    filename = 'composite_seed_' + str(settings['seed']) + '.png'
    tmp.save(filename)
    print('>> result written to: ' + filename)

##############################################################################
# automatic backup of in-progress render
##############################################################################

def backup_render(im_sett, initial_test=False):
    """
    backup the render directory to a user specified location

    typically the destination held in settings['auto_backup'] would be either
    the default '' (do not backup), or a user setting, usually in the
    form of user@host:directory

    if we are performing the initial test backup (initial_test=True) add
    --delete to the rsync options so we clear out the blocks from the previous
    render, to avoid confusion

    rsync options:
    a    --archive     archive mode
    c    --checksum    skip based on checksum, not mod-time & size
    q    --quiet       suppress non-error messages
         --delete      delete files from the destination directory that are
                       not present in source directory
    --------------------------------------------------------------------------
    args
        settings : image_config dictionary
            contains core information about the image to be rendered
        initial_test : bool
            flag to indicate if this is the initial test backup after reading
            the user configuration file, or a normal backup during the render
    --------------------------------------------------------------------------
    returns : none
    --------------------------------------------------------------------------
    """
    if im_sett['auto_backup']:
        init_del = ' --delete ' if initial_test else ''
        transfer_files = ' '.join(['rsync -acq', init_del, '"$PWD"', '"' + \
            im_sett['auto_backup'] + '"'])

        # the shell is needed to parse $PWD
        transfer_failed = subprocess.call(transfer_files, shell=True, \
            stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

        if transfer_failed:
            print('automatic backup failed, rsync returned ' + str(transfer_failed))

            # only terminate the script if this is the initial test
            # otherwise the script should continue to manage the rendering process
            if initial_test:
                sys.exit('initial backup test failed, exiting')


##############################################################################
# distribute files to render nodes
#
#    * blender file
#        any supporting blender library files
#        any associated textures
#    * python script to configure blender to render just the block required
#
##############################################################################

def _write_blender_python_config(settings, block_number):
    """
    write configuration python script to instruct blender to render a single
    block using border rendering

    assert sensible defaults to minimise render time in the context of a
    distributed render

    --------------------------------------------------------------------------
    args
        settings : image_config dictionary
            contains core information about the image to be rendered
        block_number : int
            block number to render
    --------------------------------------------------------------------------
    returns : none
    --------------------------------------------------------------------------
    """
    block_coords = utils.render_block_normal(settings, block_number)
    commands = '\n'.join([
        'import bpy',
        'scene=bpy.context.scene',
        # enable border rendering so we can define the area of the block to be
        # rendered
        'scene.render.use_border=True',
        # set the output filename
        'scene.render.filepath="//' + utils.block_render_filename(settings, block_number) + '"',
        # set the noise seed
        'scene.cycles.seed=' + str(settings['seed']),
        # the default on blender versions earlier than 2.77 is to render from the
        # centre outwards - which is fine for viewport renders - but
        # is slightly slower than methods that render adjacent tiles in order
        'scene.cycles.tile_order="BOTTOM_TO_TOP"',
        # tends to give small performance improvements for renders where
        # block render time >> scene build time
        'scene.cycles.debug_use_spatial_splits=' + str(settings['spatial_splits']),
        # make sure progressive refine is disabled
        'scene.cycles.use_progressive_refine=False',
        # set number of threads to automatic
        'scene.render.threads_mode="AUTO"',
        # set the block boundaries
        'scene.render.border_min_x=' + str(block_coords[0]),
        'scene.render.border_min_y=' + str(block_coords[1]),
        'scene.render.border_max_x=' + str(block_coords[2]),
        'scene.render.border_max_y=' + str(block_coords[3]),
        # set the tile size for Blender to that used by the script for
        # calculating the optimal block arrangement
        'scene.render.tile_x=' + str(settings['tile_size_x']),
        'scene.render.tile_y=' + str(settings['tile_size_y']),
        # set the image resolution
        'scene.render.resolution_x=' + str(settings['image_x']),
        'scene.render.resolution_y=' + str(settings['image_y']),
        'scene.render.resolution_percentage=100',
        # set filetype
        'scene.render.image_settings.file_format="PNG"',
        'scene.render.image_settings.color_mode="RGBA"',
        'scene.render.image_settings.compression=100',
        # set frame
        'scene.frame_set(' + str(settings['frame']) + ')',
        # post processing should always be suppressed for block renders
        'scene.render.use_compositing=False',
        'scene.render.use_sequencer=False',
        # instruction to render the image
        'bpy.ops.render.render(write_still=True)'])

    filename = ds.FILENAME_CONFIG_BLENDER + str(block_number).zfill(settings['padding']) + '.py'
    with open(filename, 'w') as configuration:
        configuration.write(commands + '\n')

def distribute_files_to_node(settings, node, rt_block):
    """
    before initiating the asynchronous render, make sure the node has all the
    files it needs

    rsync options used:
    -a    archive mode
    -c    compare files based on checksum
    -q    quiet

    --------------------------------------------------------------------------
    args
        settings : image_config dictionary
            contains core information about the image to be rendered
        node : instance of RenderNode
            contains information about the node we will send a block to
        rt_block : int
           the number of the block to be rendered
    --------------------------------------------------------------------------
    returns : none
    --------------------------------------------------------------------------
    """
    # write a blender config file and copy to the node
    _write_blender_python_config(settings, rt_block)
    local_filename = ds.FILENAME_CONFIG_BLENDER + str(rt_block).zfill(settings['padding']) + '.py'
    remote_filename = node.username + '@' + node.ip_address + ':~/' + local_filename
    transfer_files = ' '.join(['rsync -acq', local_filename, remote_filename])
    subprocess.call(transfer_files, shell=True, \
        stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

    # if this is the first block the node has received, send the render files
    # too.
    if node.first_use:
        remote_filename = node.username + '@' + node.ip_address + ':~'

        # render file
        subprocess.call(['rsync', '-acq', 'render.blend', remote_filename], \
            stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

        # textures directory
        if os.path.isdir(settings['textures_directory']):
            subprocess.call(['rsync', '-acq', settings['textures_directory'], remote_filename], \
                stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

        # library directory
        if os.path.isdir(settings['library_directory']):
            subprocess.call(['rsync', '-acq', settings['library_directory'], remote_filename], \
                stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

        node.first_use = False
